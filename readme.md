This repository contains a list of articles in the [GCC Wiki](https://gcc.gnu.org/wiki/) (updated 05.03.15). It is aimed to help sorting and systematizing the content.
The scripts directory contains third-party components used for displaying a sortable table:

* [jQuery](https://jquery.com) library, Copyright (c) 2015 The jQuery Foundation, distributed under MIT license
* [TableSorter](http://tablesorter.com) jQuery plugin, Copyright (c) 2007 Christian Bach, Dual licensed under the MIT and GPL licenses